import React from 'react';
import PropTypes from 'prop-types';

const PostHeader = (props) => {
  const { avatar, username, publishedIn } = props;
  return (
    <div className="post-header">
      <img className="avatar" src={avatar} alt={username} />
      <div className="flexbox">
        <div className="username">
          {username}
        </div>
        <div className="published-in">
          {publishedIn}
        </div>
      </div>
    </div>
  );
};

PostHeader.defaultProps = {
  avatar: '',
  username: '',
  publishedIn: '',
};

PostHeader.propTypes = {
  avatar: PropTypes.string,
  username: PropTypes.string,
  publishedIn: PropTypes.string,
};

export default PostHeader;
