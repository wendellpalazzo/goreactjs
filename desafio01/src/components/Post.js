import React from 'react';
import PropTypes from 'prop-types';
import PostHeader from './PostHeader';

const Post = (props) => {
  const { data } = props;
  return (
    <div className="container">
      {data.map(p => (
        <div className="post">
          <PostHeader avatar={p.avatar} username={p.username} publishedIn={p.publishedIn} />
          <hr className="separator" />
          {p.contentText}
        </div>
      ))}
    </div>
  );
};

Post.defaultProps = {
  data: [],
};

Post.propTypes = {
  data: PropTypes.arrayOf('Object'),
};

export default Post;
